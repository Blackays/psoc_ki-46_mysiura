/*******************************************************************************
* File Name: LCD_Mych_PM.c
* Version 2.20
*
* Description:
*  This file provides the API source code for the Static Segment LCD component.
*
* Note:
*
********************************************************************************
* Copyright 2008-2014, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "LCD_Mych.h"


static LCD_Mych_BACKUP_STRUCT LCD_Mych_backup;


/*******************************************************************************
* Function Name: LCD_Mych_SaveConfig
********************************************************************************
*
* Summary:
*  Does nothing, provided for consistency.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
*******************************************************************************/
void LCD_Mych_SaveConfig(void) 
{
}


/*******************************************************************************
* Function Name: LCD_Mych_RestoreConfig
********************************************************************************
*
* Summary:
*  Does nothing, provided for consistency.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
*******************************************************************************/
void LCD_Mych_RestoreConfig(void) 
{
}


/*******************************************************************************
* Function Name: LCD_Mych_Sleep
********************************************************************************
*
* Summary:
*  Prepares the component for entering the sleep mode.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
* Reentrant:
*  No.
*
*******************************************************************************/
void LCD_Mych_Sleep(void) 
{
    LCD_Mych_backup.enableState = LCD_Mych_enableState;
    LCD_Mych_SaveConfig();
    LCD_Mych_Stop();
}


/*******************************************************************************
* Function Name: LCD_Mych_Wakeup
********************************************************************************
*
* Summary:
*  Wakes the component from sleep mode. Configures DMA and enables the component
*  for normal operation.
*
* Parameters:
*  LCD_Mych_enableState - Global variable.
*
* Return:
*  Status one of standard status for PSoC3 Component
*       CYRET_SUCCESS - Function completed successfully.
*       CYRET_LOCKED - The object was locked, already in use. Some of TDs or
*                      a channel already in use.
*
* Reentrant:
*  No.
*
*******************************************************************************/
void LCD_Mych_Wakeup(void) 
{
    LCD_Mych_RestoreConfig();

    if(LCD_Mych_backup.enableState == 1u)
    {
        LCD_Mych_Enable();
    }
}


/* [] END OF FILE */

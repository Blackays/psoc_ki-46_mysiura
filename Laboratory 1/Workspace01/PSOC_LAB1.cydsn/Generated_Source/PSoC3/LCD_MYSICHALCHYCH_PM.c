/*******************************************************************************
* File Name: LCD_MYSICHALCHYCH_PM.c
* Version 2.20
*
* Description:
*  This file provides the API source code for the Static Segment LCD component.
*
* Note:
*
********************************************************************************
* Copyright 2008-2014, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "LCD_MYSICHALCHYCH.h"


static LCD_MYSICHALCHYCH_BACKUP_STRUCT LCD_MYSICHALCHYCH_backup;


/*******************************************************************************
* Function Name: LCD_MYSICHALCHYCH_SaveConfig
********************************************************************************
*
* Summary:
*  Does nothing, provided for consistency.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
*******************************************************************************/
void LCD_MYSICHALCHYCH_SaveConfig(void) 
{
}


/*******************************************************************************
* Function Name: LCD_MYSICHALCHYCH_RestoreConfig
********************************************************************************
*
* Summary:
*  Does nothing, provided for consistency.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
*******************************************************************************/
void LCD_MYSICHALCHYCH_RestoreConfig(void) 
{
}


/*******************************************************************************
* Function Name: LCD_MYSICHALCHYCH_Sleep
********************************************************************************
*
* Summary:
*  Prepares the component for entering the sleep mode.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
* Reentrant:
*  No.
*
*******************************************************************************/
void LCD_MYSICHALCHYCH_Sleep(void) 
{
    LCD_MYSICHALCHYCH_backup.enableState = LCD_MYSICHALCHYCH_enableState;
    LCD_MYSICHALCHYCH_SaveConfig();
    LCD_MYSICHALCHYCH_Stop();
}


/*******************************************************************************
* Function Name: LCD_MYSICHALCHYCH_Wakeup
********************************************************************************
*
* Summary:
*  Wakes the component from sleep mode. Configures DMA and enables the component
*  for normal operation.
*
* Parameters:
*  LCD_MYSICHALCHYCH_enableState - Global variable.
*
* Return:
*  Status one of standard status for PSoC3 Component
*       CYRET_SUCCESS - Function completed successfully.
*       CYRET_LOCKED - The object was locked, already in use. Some of TDs or
*                      a channel already in use.
*
* Reentrant:
*  No.
*
*******************************************************************************/
void LCD_MYSICHALCHYCH_Wakeup(void) 
{
    LCD_MYSICHALCHYCH_RestoreConfig();

    if(LCD_MYSICHALCHYCH_backup.enableState == 1u)
    {
        LCD_MYSICHALCHYCH_Enable();
    }
}


/* [] END OF FILE */
